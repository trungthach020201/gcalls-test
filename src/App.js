import "./App.css";
import Numpad from "./components/Numpad";

function App() {
  return <Numpad />;
}

export default App;
