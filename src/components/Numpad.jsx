import React, { useState } from "react";
import "./styles.scss";
import JsSIP from "jssip";
function Numpad() {
  const [phone, setNumber] = useState("");

  const handleClick = (value) => {
    setNumber((prevNumber) => prevNumber + value);
  };

  var socket = new JsSIP.WebSocketInterface("wss://gc03-pbx.tel4vn.com:7444");
  var configuration = {
    sockets: [socket],
    uri: "sip:105@2-test1.gcalls.vn:50061",
    password: "test1105",
    session_timers: false,
  };
  var userAgent = new JsSIP.UA(configuration);

  function Call() {
    var phoneNumberInput = document.getElementById("phoneNumber");
    var phoneNumber = phoneNumberInput.value;
    // Create a new JSSIP User Agent instance

    userAgent.start();

    // Make the call
    var session = userAgent.call(phoneNumber);

    // Handle call events
    session.on("connecting", function () {
      console.log("Connecting...");
    });

    session.on("connected", function () {
      console.log("Connected.");
    });

    session.on("failed", function () {
      console.log("Call failed.");
    });

    // Handle incoming audio stream
    session.connection.addEventListener("addstream", function (e) {
      var audio = new Audio();
      audio.srcObject = e.stream;
      audio.play();
    });

    const iceServers = [
      { urls: "stun:stun.l.google.com:19302" },
      { urls: "stun:stun1.l.google.com:19302" },
      { urls: "stun:stun2.l.google.com:19302" },
    ];
  }

  function Endcall() {
    var session = userAgent.terminateSessions();
    session.on("ended", function () {
      console.log("Call ended.");
    });
  }

  return (
    <div className="container">
      <div className="numpad">
        <h2>Call Center</h2>
        <input id="phoneNumber" type="text" value={phone} />
        <br />
        <div className="numbers">
          <button className="btn-num" onClick={() => handleClick(1)}>
            1
          </button>
          <button className="btn-num" onClick={() => handleClick(2)}>
            2
          </button>
          <button className="btn-num" onClick={() => handleClick(3)}>
            3
          </button>
          <br />
          <button className="btn-num" onClick={() => handleClick(4)}>
            4
          </button>
          <button className="btn-num" onClick={() => handleClick(5)}>
            5
          </button>
          <button className="btn-num" onClick={() => handleClick(6)}>
            6
          </button>
          <br />
          <button className="btn-num" onClick={() => handleClick(7)}>
            7
          </button>
          <button className="btn-num" onClick={() => handleClick(8)}>
            8
          </button>
          <button className="btn-num" onClick={() => handleClick(9)}>
            9
          </button>
          <br />
          <button className="btn-num" onClick={() => handleClick("*")}>
            *
          </button>
          <button className="btn-num" onClick={() => handleClick(0)}>
            0
          </button>
          <button className="btn-num" onClick={() => handleClick("#")}>
            #
          </button>
        </div>
        <div>
          <button className="btn-call" onClick={() => Call()}>
            Call
          </button>
          <button className="btn-end" onClick={() => Endcall()}>
            End Call
          </button>
        </div>
      </div>
    </div>
  );
}

export default Numpad;
